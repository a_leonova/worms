package ru.nsu.g16205.leonova.gameObjects;

import javafx.scene.image.Image;
import ru.nsu.g16205.leonova.GameStandardOptions;
import ru.nsu.g16205.leonova.factory.Factory;
import ru.nsu.g16205.leonova.physics.*;
import ru.nsu.g16205.leonova.view.GraphicObject;

import java.awt.image.BufferedImage;

public class UsualBullet implements AwaitableObject, PhysicObject, GraphicObject {

    private boolean alive;
    private PixelCollider collider;
    private Speed speed;
    private Position position;
    private int damage;
    private static Image image;
    private int rad;
    private boolean onGround;
    private boolean isRightDirection = true;
    private int layer = 2;
    static {
        image = new Image("bullet.png");
    }

    public UsualBullet(Position newPosition, Speed speed){
        alive = true;
        damage = GameStandardOptions.stdUsualBulletDamage;
        rad = GameStandardOptions.airBlastRad;
        position = new Position(newPosition);
        this.speed = speed;
        collider = new BitmapCollider(image);
    }

    @Override
    public boolean isAlive() {
        return alive;
    }

    @Override
    public boolean isPushOut() {
        return false;
    }

    @Override
    public boolean isEjected() {
        return true;
    }

    @Override
    public boolean onGround() {
        return onGround;
    }

    @Override
    public boolean isMoving() {
        return speed.getVx() != 0 || speed.getVy() != 0;
    }

    @Override
    public void setOnGround(boolean value) {
        onGround = value;
    }

    @Override
    public void setPosition(Position newPosition) {
        position.setPosition(newPosition);
    }

    @Override
    public void setPosition(double x, double y) {
        position.setPosition(x,y);
    }

    @Override
    public void onCollision(PhysicObject other) {
       if(alive){
           Factory.getInstance().create(AirBlast.class, rad, damage, positionForAirBlast(position));
           alive = false;
           if (other.onGround()){
               onGround = true;
           }
       }
    }

    @Override
    public PixelCollider getCollider() {
        return collider;
    }

    @Override
    public Position getPosition() {
        return position;
    }

    @Override
    public int getWidth() {
        return collider.getWidth();
    }

    @Override
    public int getHeight() {
        return collider.getHeight();
    }

    @Override
    public Image getImage() {
        return image;
    }

    @Override
    public int getLayer() {
        return layer;
    }

    @Override
    public boolean isRight() {
        return isRightDirection;
    }

    @Override
    public Speed getSpeed() {
        return speed;
    }

    @Override
    public void oneIterationGone() {

    }

    @Override
    public boolean isGravity() {
        return true;
    }

    private Position positionForAirBlast(Position pos){
        double newX = pos.getX() + getWidth() / 2 - rad;
        double newY = pos.getY() - getHeight() / 2 + rad;
        return new Position(newX, newY);
    }
}
