package ru.nsu.g16205.leonova.gameObjects;

import javafx.geometry.Pos;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.transform.Affine;
import ru.nsu.g16205.leonova.GameStandardOptions;
import ru.nsu.g16205.leonova.factory.Factory;
import ru.nsu.g16205.leonova.physics.*;
import ru.nsu.g16205.leonova.view.GraphicObject;
import ru.nsu.g16205.leonova.view.ImageTransformation;

public class UsualWorm implements Worm, PhysicObject, GraphicObject, DamageRecipient, WormInformation {

    private static Image image;
    private boolean alive;
    private int lives;
    private Weapon weapon;
    private Speed speed;
    private Position position;
    private PixelCollider colliderRight;
    private PixelCollider colliderLeft;
    private PixelCollider collider;
    private boolean onGround;
    private boolean isRightDirection = true;
    private int layer = 1;
    private String name = "ololo";

    static {
        image = new Image("0Worm.png");
    }

    public UsualWorm(Integer livesValue, Position newPosition, String newName){
        lives = livesValue;
        alive = true;
        speed = new Speed();
        position = new Position(newPosition);
        colliderRight = new BitmapCollider(image);
        colliderLeft = new BitmapCollider(ImageTransformation.flip(image));
        collider = colliderRight;
        weapon = Factory.getInstance().create(Bazooka.class, new Position(newPosition.getX() + image.getWidth()/2,
                newPosition.getY() - image.getHeight()/2));
        name = newName;

    }

    public boolean isAlive(){
        return alive;
    }

    @Override
    public boolean isPushOut() {
        return true;
    }

    @Override
    public boolean isEjected() {
        return true;
    }

    @Override
    public boolean onGround() {
        return onGround;
    }

    @Override
    public boolean isMoving() {
        return speed.getVx() != 0 || speed.getVy() != 0;
    }

    @Override
    public void setOnGround(boolean value) {
        onGround = value;
    }

    @Override
    public void setPosition(Position newPosition) {
        position.setPosition(newPosition);
        weapon.setPosition(cntPositionForWeapon());
    }

    @Override
    public void setPosition(double x, double y) {
        position.setPosition(x, y);
        weapon.setPosition(cntPositionForWeapon());
    }

    @Override
    public boolean increasePower() {
        return weapon.increasePower();
    }

    @Override
    public void active() {
        weapon.active();
    }

    @Override
    public void disactive() {
        weapon.disactive();
    }

    @Override
    public void addLives(int value){
        if (lives + value < 0){
            lives = 0;
            die();
        }
        else{
            lives += value;
        }
    }

    @Override
    public AwaitableObject shoot(){
        return weapon.shoot();
    }

    @Override
    public void jump(Speed jmpSpeed){
        if(onGround){
            if (!isRightDirection){
                jmpSpeed.setVx(-jmpSpeed.getVx());
            }
            speed.setSpeed(jmpSpeed);
        }
    }

    @Override
    public void move(Speed speed){
        if(onGround){
            isRightDirection = speed.getVx() >= 0;
            collider = isRightDirection ? colliderRight : colliderLeft;
            weapon.changeDirection(isRightDirection);
            this.speed.setSpeed(speed);
        }
    }

    @Override
    public void weaponUp() {
        weapon.weaponUp();
    }

    @Override
    public void weaponDown() {
        weapon.weaponDown();
    }

    @Override
    public PixelCollider getCollider() {
        return collider;
    }

    @Override
    public Position getPosition() {
        return position;
    }

    @Override
    public int getWidth() {
        return collider.getWidth();
    }

    @Override
    public int getHeight() {
        return collider.getHeight();
    }

    @Override
    public Image getImage() {

        if(!isRightDirection){
            Image newImage = ImageTransformation.flip(image);
            return newImage;
        }
        return image;
    }

    @Override
    public int getLayer() {
        return layer;
    }

    @Override
    public boolean isRight() {
        return isRightDirection;
    }

    @Override
    public Speed getSpeed() {
        return speed;
    }

    @Override
    public void oneIterationGone() {

    }

    @Override
    public boolean isGravity() {
        return true;
    }

    @Override
    public void onCollision(PhysicObject other) {
        //AirBlast do itself when collide with worm
        if(other.onGround() && !other.isMoving()){
            onGround = true;
        }else{
            onGround = false;
        }
    }

    private void die(){
        weapon.setIsAlive(false);
        alive = false;
    }

    @Override
    public void getDamage(int value) {
        lives = lives - value < 0 ? 0 : lives - value;
        if(lives == 0)
            die();
    }

    private Position cntPositionForWeapon(){
        double middleX = position.getX() + this.collider.getWidth() * GameStandardOptions.weaponXShift;
        double middleY = position.getY() - this.collider.getHeight() * GameStandardOptions.weaponYShift;

        Position shift = weapon.getShiftToCenter();
        //System.out.println("shift: " + shift.getX() + " " + shift.getY());

        return new Position(middleX - shift.getX(), middleY + shift.getY());
    }

    @Override
    public int getLives() {
        return lives;
    }

    @Override
    public String getName() {
        return name;
    }
}
