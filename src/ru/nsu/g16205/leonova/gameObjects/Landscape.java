package ru.nsu.g16205.leonova.gameObjects;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import ru.nsu.g16205.leonova.physics.*;
import ru.nsu.g16205.leonova.view.GraphicObject;

import java.awt.*;
import java.awt.image.BufferedImage;

public class Landscape implements PhysicObject, GraphicObject {

    private PixelCollider collider;
    private Speed speed;
    private Position position;
    private static Image image;
    private int layer = 0;

    static {
        image = new Image("gr.png");
    }

    public Landscape(Position newPosition) {
        speed = new Speed(0.0, 0.0);
        position = new Position(newPosition);
        collider = new BitmapCollider(image);
    }


    @Override
    public void onCollision(PhysicObject other) {
        if (other instanceof AirBlast) {
            destroyLand(other.getPosition(), other.getCollider().getHeight() / 2);
        }
    }

    @Override
    public PixelCollider getCollider() {
        return collider;
    }

    @Override
    public Position getPosition() {
        return position;
    }

    @Override
    public int getWidth() {
        return collider.getWidth();
    }

    @Override
    public int getHeight() {
        return collider.getHeight();
    }

    @Override
    public Image getImage() {
        return image;
    }

    @Override
    public int getLayer() {
        return layer;
    }

    @Override
    public boolean isRight() {
        return true;
    }

    @Override
    public Speed getSpeed() {
        return speed;
    }

    @Override
    public void oneIterationGone() {

    }

    @Override
    public boolean isGravity() {
        return false;
    }

    @Override
    public boolean isAlive() {
        return true;
    }

    @Override
    public boolean isPushOut() {
        return true;
    }

    @Override
    public boolean isEjected() {
        return false;
    }

    @Override
    public boolean onGround() {
        return true;
    }

    @Override
    public boolean isMoving() {
        return false;
    }

    @Override
    public void setOnGround(boolean value) {
    }

    //landscape doesn't need to be moved
    @Override
    public void setPosition(Position newPosition) {
    }

    @Override
    public void setPosition(double x, double y) {

    }

    private void destroyLand(Position startPos, int rad) {
        //find bounds

        BufferedImage buffImg = SwingFXUtils.fromFXImage(image, null);
        Color color = new Color(0.0f, 0.0f, 0.0f, 0.0f);
        int rgb = color.getRGB();

        int startWidth = (int) startPos.getX() > 0 ? (int) startPos.getX() : 0;
        int endWidth = (int) startPos.getX() + 2 * rad < this.getWidth() ? (int) startPos.getX() + 2 *rad: this.getWidth();
        int startHeight = (int) startPos.getY() < 0 ? (int) (startPos.getY()) * -1 : 0;
        int endHeight = (int) startPos.getY() - 2 * rad > -1 * this.getHeight() ? ((int) startPos.getY() - 2 * rad) * -1
                : this.getHeight();

        //check pixels, which in defeat
        for (int i = startHeight; i < endHeight; ++i) {
            for (int j = startWidth; j < endWidth; ++j) {
                double x = j - (startPos.getX() + rad);
                double y = i - -1 * (startPos.getY() - rad);

                if (x * x + y * y <= rad * rad) {
                    collider.setPixel(j, i, false);
                    buffImg.setRGB(j, i, rgb);
                }
            }
        }

        image = SwingFXUtils.toFXImage(buffImg, null);
    }

}
