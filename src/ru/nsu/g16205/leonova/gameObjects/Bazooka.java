package ru.nsu.g16205.leonova.gameObjects;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.transform.Affine;
import ru.nsu.g16205.leonova.GameStandardOptions;
import ru.nsu.g16205.leonova.factory.Factory;
import ru.nsu.g16205.leonova.physics.*;
import ru.nsu.g16205.leonova.view.GraphicObject;
import ru.nsu.g16205.leonova.view.ImageTransformation;

import java.util.ArrayList;

public class Bazooka implements Weapon, GraphicObject {

    private Position position;
    private static Image bazookaWithoutAim;;
    private static Image bazookaWithAim;;
    private Image imageToShow;
    private double power;
    private double angleRad = 0.0;
    private int layer = 2;
    private boolean isAlive;
    private boolean isRightDirection = true;

    private int xLeftShift = 0;
    static {
        bazookaWithAim = new Image("0bazookaWithAim.png");
        bazookaWithoutAim = new Image("bazookaWithoutAim.png");
    }

    public Bazooka(Position newPosition){
        position = new Position(newPosition);
        power = 0;
        imageToShow = bazookaWithoutAim;
        isAlive = true;
    }

    @Override
    public AwaitableObject shoot() {
        double angle = isRightDirection ? angleRad : Math.PI - angleRad;
        double newVx = GameStandardOptions.speedForBullet * Math.cos(angle) * power;
        double newVy = GameStandardOptions.speedForBullet * Math.sin(angle) * power;
        UsualBullet bullet = Factory.getInstance().create(UsualBullet.class, cntPositionForBullet(),
                new Speed(newVx, newVy));
        power = 0;
        return bullet;
    }

    @Override
    public void weaponUp() {
        angleRad += Math.PI / 180;
        if (angleRad > Math.PI / 2){
            angleRad = Math.PI / 2;
        }
    }

    @Override
    public void weaponDown() {
        angleRad -= Math.PI/180;
        if (angleRad < -Math.PI / 2){
            angleRad = -Math.PI / 2;
        }
    }

    @Override
    public boolean increasePower() {
        power += GameStandardOptions.powerEnlargement;
        return power >= GameStandardOptions.maxShotPower;
    }

    @Override
    public void active() {
        imageToShow = bazookaWithAim;
    }

    @Override
    public void disactive() {
        imageToShow = bazookaWithoutAim;
    }

    public Position getPosition() {
        return position;
    }

    @Override
    public int getWidth() {
        return (int) imageToShow.getWidth();
    }

    @Override
    public int getHeight() {
        return (int) imageToShow.getHeight();
    }

    @Override
    public Image getImage() {

        Image newImage = imageToShow;
        if(!isRightDirection){

           // double newX
            //System.out.println("Image: " + newImage.getWidth()+ " " + newImage.getHeight());
            newImage = ImageTransformation.flip(newImage);
            newImage = ImageTransformation.changeAngle(newImage, angleRad);
//            double newX = position.getX() - imageToShow.getWidth();
//            position.setPosition(newX, position.getY());
            xLeftShift = -20;
            return newImage;
        }
        xLeftShift = 0;
        newImage = ImageTransformation.changeAngle(imageToShow, -angleRad);
        return newImage;
    }

    @Override
    public int getLayer() {
        return layer;
    }

    @Override
    public boolean isRight() {
        return isRightDirection;
    }

    @Override
    public boolean isAlive() {
        return isAlive;
    }

    public void setPosition(Position newPosition){
        position.setPosition(newPosition.getX() + xLeftShift,
                newPosition.getY());
    }

    @Override
    public void setPosition(double x, double y) {
        position.setPosition(x + xLeftShift,y);
    }

    @Override
    public void changeDirection(boolean value) {
        isRightDirection = value;
    }

    @Override
    public Position getShiftToCenter() {
        return new Position(imageToShow.getWidth() / 2, imageToShow.getHeight() / 2);
    }

    @Override
    public void setIsAlive(boolean value) {
        isAlive = value;
    }

    private Position cntPositionForBullet(){
        int sign = isRightDirection ? 1 : -1;
        double newX = position.getX() + getWidth()/2 + sign * GameStandardOptions.distFromcenterToLanch * Math.cos(angleRad);
        double newY = position.getY() - getHeight()/2 + GameStandardOptions.distFromcenterToLanch*Math.sin(angleRad);
        return new Position(newX, newY);
    }


}
