package ru.nsu.g16205.leonova.gameObjects;

import ru.nsu.g16205.leonova.physics.Position;

public interface WormInformation {
    int getLives();
    String getName();
    Position getPosition();
    boolean isAlive();
}
