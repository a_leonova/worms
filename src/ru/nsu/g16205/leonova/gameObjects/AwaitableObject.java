package ru.nsu.g16205.leonova.gameObjects;

public interface AwaitableObject {
    boolean isAlive();
}
