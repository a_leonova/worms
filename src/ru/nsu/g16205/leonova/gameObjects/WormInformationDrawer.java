package ru.nsu.g16205.leonova.gameObjects;

import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import ru.nsu.g16205.leonova.GameStandardOptions;
import ru.nsu.g16205.leonova.physics.Position;
import ru.nsu.g16205.leonova.view.GraphicObject;
import ru.nsu.g16205.leonova.view.ImageTransformation;

public class WormInformationDrawer implements GraphicObject{

    private WormInformation information;
    private int layer = 2;
    private Image image;
    private Color color;

    public WormInformationDrawer(WormInformation wi, Color oColor){
        information = wi;
        color = oColor;
    }

    @Override
    public Position getPosition() {
        Position wormPos = information.getPosition();
        double newX = wormPos.getX();
        double newY = wormPos.getY() + GameStandardOptions.shiftYForInfo;
        return new Position(newX, newY);
    }

    @Override
    public int getWidth() {
        return (int)image.getWidth();
    }

    @Override
    public int getHeight() {
        return (int)image.getHeight();
    }

    @Override
    public Image getImage() {
        String info = information.getName() + ": " + information.getLives();
        image = textToImage(info);
        return image;
    }

    @Override
    public int getLayer() {
        return layer;
    }

    @Override
    public boolean isRight() {
        return true;
    }

    @Override
    public boolean isAlive() {
        return information.isAlive();
    }

    private WritableImage textToImage(String text){

        SnapshotParameters sp = new SnapshotParameters();
        sp.setFill(Color.TRANSPARENT);

        Text t = new Text(text);
        t.setFill(color);
        t.setFont(Font.loadFont("file:resource/fonts/Amiri-Bold.ttf", 18));
        Scene scene = new Scene(new StackPane(t));
        return t.snapshot(sp, null);

    }
}
