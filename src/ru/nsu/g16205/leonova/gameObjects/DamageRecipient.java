package ru.nsu.g16205.leonova.gameObjects;

public interface DamageRecipient {
    void getDamage(int value);
}
