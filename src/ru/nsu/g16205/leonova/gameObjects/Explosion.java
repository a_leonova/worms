package ru.nsu.g16205.leonova.gameObjects;

import javafx.scene.image.Image;
import ru.nsu.g16205.leonova.GameStandardOptions;
import ru.nsu.g16205.leonova.physics.Position;
import ru.nsu.g16205.leonova.view.GraphicObject;

import java.util.ArrayList;

public class Explosion implements GraphicObject {


    private Position position;
    private double height;
    private double width;
    private int idxOfShownImage = 0;
    private int cnt = 0;
    private boolean alive = true;
    private int layer = 2;
    private static ArrayList<Image> images;

    static {
        images = new ArrayList<>(12);
        for(int i = 1; i <= 12; ++i){
            String name ="/blow/" + Integer.toString(i) + ".png";
            images.add(new Image(name));
        }
    }

    public Explosion(Position center){
        height = images.get(0).getHeight();
        width = images.get(0).getWidth();
        double newX = center.getX() - width / 2;
        double newY = center.getY() + height / 2;
        position = new Position(newX, newY);
    }


    @Override
    public Position getPosition() {
        return position;
    }

    @Override
    public int getWidth() {
        return (int)width;
    }

    @Override
    public int getHeight() {
        return (int)height;
    }

    @Override
    public Image getImage() {

        Image imageToShow = images.get(idxOfShownImage);

        if(cnt++ > GameStandardOptions.duration){
            cnt = 0;
            alive = ++idxOfShownImage < images.size();
        }

        return imageToShow;
    }

    @Override
    public int getLayer() {
        return layer;
    }

    @Override
    public boolean isRight() {
        return true;
    }

    @Override
    public boolean isAlive() {
        return alive;
    }
}
