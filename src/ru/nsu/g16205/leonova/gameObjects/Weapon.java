package ru.nsu.g16205.leonova.gameObjects;

import ru.nsu.g16205.leonova.gameObjects.AwaitableObject;
import ru.nsu.g16205.leonova.physics.Position;

public interface Weapon {

    AwaitableObject shoot();
    void weaponUp();
    void weaponDown();
    boolean increasePower();
    void active();
    void disactive();
    void setPosition(Position newPosition);
    void setPosition(double x, double y);
    void changeDirection(boolean value);
    Position getShiftToCenter();
    void setIsAlive(boolean value);

}
