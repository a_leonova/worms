package ru.nsu.g16205.leonova.gameObjects;

import ru.nsu.g16205.leonova.gameObjects.AwaitableObject;
import ru.nsu.g16205.leonova.physics.Speed;

public interface Worm {
    void addLives(int value);
    AwaitableObject shoot();
    void jump(Speed jmpSpeed);
    void move(Speed speed);
    void weaponUp();
    void weaponDown();
    boolean isAlive();
    boolean increasePower();
    void active();
    void disactive();

}
