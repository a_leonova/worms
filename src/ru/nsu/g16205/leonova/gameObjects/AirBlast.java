package ru.nsu.g16205.leonova.gameObjects;

import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import ru.nsu.g16205.leonova.GameStandardOptions;
import ru.nsu.g16205.leonova.factory.Factory;
import ru.nsu.g16205.leonova.physics.*;
import ru.nsu.g16205.leonova.view.GraphicObject;

import java.util.ArrayList;
import java.util.List;

public class AirBlast implements PhysicObject {

    private boolean isAlive;
    private int damage;
    private int radius;
    private CircleCollider collider;
    private Position position;
    private Speed speed;
    private boolean onGround = false;
    private Image image;


    public AirBlast(Integer r, Integer d, Position pos){
        isAlive = true ;
        radius = r;
        damage = d;
        position = pos;
        speed = new Speed(0, 0);
        collider = new CircleCollider(radius);
        Factory.getInstance().create(Explosion.class, new Position(position.getX() + radius,
                position.getY() - radius));
        //image = createImage();
    }

    @Override
    public void onCollision(PhysicObject physicObjects) {
        if(physicObjects instanceof DamageRecipient){
            double dist = Math.sqrt(Math.pow((physicObjects.getCenter().getX() - getCenter().getX()), 2) +
                    Math.pow((physicObjects.getCenter().getY() - getCenter().getY()), 2));

            int dam = (int)(damage *(1 -  dist / radius));
            dam = dam < 0 ? 0 : dam;
            ((DamageRecipient) physicObjects).getDamage(dam);
        }
        else if (physicObjects.onGround()){
            onGround = true;
        }
        //if physicObject instanceof Landscape, then landscape will destroy itself
        //because Landscape.onCollision will be called
    }

    @Override
    public PixelCollider getCollider() {
        return collider;
    }

    @Override
    public Position getPosition() {
        return position;
    }

    @Override
    public Speed getSpeed() {
        return speed;
    }

    @Override
    public void oneIterationGone() {
        isAlive = false;
    }

    @Override
    public boolean isGravity() {
        return false;
    }

    @Override
    public boolean isAlive() {
        return isAlive;
    }

    @Override
    public boolean isPushOut() {
        return false;
    }

    @Override
    public boolean isEjected() {
        return false;
    }

    @Override
    public boolean onGround() {
        return onGround;
    }

    @Override
    public boolean isMoving() {
        return false;
    }

    @Override
    public void setOnGround(boolean value) {
        onGround = value;
    }

    @Override
    public void setPosition(Position newPosition) {
        position.setPosition(newPosition);
    }

    @Override
    public void setPosition(double x, double y) {
        position.setPosition(x, y);
    }

    private Image createImage(){
        Circle circle = new Circle(radius);
        circle.setFill(Color.RED);
        SnapshotParameters sp = new SnapshotParameters();
        sp.setFill(Color.TRANSPARENT);
        return circle.snapshot(sp, null);

    }

}
