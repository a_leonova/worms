package ru.nsu.g16205.leonova;

import javafx.scene.paint.Color;
import javafx.stage.Screen;
import ru.nsu.g16205.leonova.factory.Factory;
import ru.nsu.g16205.leonova.gameObjects.UsualWorm;
import ru.nsu.g16205.leonova.gameObjects.Worm;
import ru.nsu.g16205.leonova.gameObjects.WormInformation;
import ru.nsu.g16205.leonova.gameObjects.WormInformationDrawer;
import ru.nsu.g16205.leonova.physics.Position;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class Team {
    private List<Worm> wormsTeam;
    private String teamName;
    private Worm activeWorm;
    private static int it = 0;
    private Color color;

    public Team(TeamInfo info){
        wormsTeam = new ArrayList<>(info.getCnt());
        teamName = info.getName();

        for (int i = 0; i < info.getCnt(); ++i){
            String name = "Worm " + Integer.toString(i + 1);
            Worm newWorm = Factory.getInstance().create(UsualWorm.class,
                    info.getLives(), startWormPos(), name);
            wormsTeam.add(i, newWorm);
            Factory.getInstance().create(WormInformationDrawer.class, newWorm, info.getColor());

        }
        activeWorm = wormsTeam.get(0);
    }

    public Worm giveActiveWorm(){
        return activeWorm;
    }

    public Worm giveNextActiveWorm(){
        int i = wormsTeam.indexOf(activeWorm);
        i -= deleteDeadWorms(i);

        activeWorm = wormsTeam.get((i + 1) % wormsTeam.size());
        return activeWorm;
    }

    public boolean isAlive(){
        deleteDeadWorms(0);
        return wormsTeam.size() > 0;
    }

    private int deleteDeadWorms(int currentIdx){
        int beforeDead = 0;
        int cnt = 0;
        Iterator<Worm> i = wormsTeam.iterator();
        while (i.hasNext()){
            ++cnt;
            Worm worm = i.next();
            if (!worm.isAlive()){
                if (cnt <= currentIdx){
                    ++beforeDead;
                }
                i.remove();
            }
        }
        return  beforeDead;
    }

    private Position startWormPos(){

//        double x = it * 100 + 100;
  //      ++it;
        //Random randomGenerator = new Random();
        double x =  GameStandardOptions.gameWidth * Math.random();
        return new Position(x, GameStandardOptions.startWormsHeight);
    }

}
