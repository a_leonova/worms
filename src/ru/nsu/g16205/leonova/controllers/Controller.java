package ru.nsu.g16205.leonova.controllers;

public interface Controller {
    void update(double deltaTime);
}
