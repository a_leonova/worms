package ru.nsu.g16205.leonova.controllers;

import com.sun.javafx.geom.Vec2d;
import ru.nsu.g16205.leonova.GameStandardOptions;
import ru.nsu.g16205.leonova.physics.*;

import java.util.ArrayList;
import java.util.List;


public class UsualPhysicController implements Physic, Controller {

    private List<PhysicObject> physicObjects;
    private final double gravityConst = GameStandardOptions.gravityConst;
    private List<PhysicObject> trialPhysicObjects; //first of all we will put PO here because it can be created
                                            // during iteration

    public UsualPhysicController(){
        physicObjects = new ArrayList<>();
        trialPhysicObjects = new ArrayList<>();
    }

    @Override
    public void addPhysicObj(PhysicObject newObject) {
        trialPhysicObjects.add(newObject);
    }

    @Override
    public List<PhysicObject> getPhysicObj() {
        return physicObjects;
    }

    @Override
    public void update(double deltaTime) {
        physicObjects.addAll(trialPhysicObjects);
        trialPhysicObjects.clear();
        deleteDeadObjects();

        //influence like physic (move, speed, etc)
        for (PhysicObject object: physicObjects) {

            if(object.isGravity() && (object.isMoving() || !object.onGround())){
                double newVy = object.getSpeed().getVy() - gravityConst * deltaTime;
                Speed newSpeed = new Speed(object.getSpeed().getVx(), newVy);
                object.getSpeed().setSpeed(newSpeed);
            }
            if(object.isMoving()){
                object.setOnGround(false);
            }

            double newX = object.getPosition().getX() + object.getSpeed().getVx() * deltaTime;
            double newY = object.getPosition().getY() + object.getSpeed().getVy() * deltaTime;
            object.setPosition(newX, newY);
            //after object moves it will be checked on collision
            collide(object);
            //airblast must be removed after 1 iteration
            object.oneIterationGone();
            checkMinSpeed(object);
        }

    }

    private void checkMinSpeed(PhysicObject object){
        Speed speed = object.getSpeed();
        if(object.onGround() &&
                speed.getVx() * speed.getVx() + speed.getVy()*speed.getVy() < GameStandardOptions.speedToStopSqr) {
            speed.setVy(0);
            speed.setVx(0);
        }
    }

    private void deleteDeadObjects(){

        physicObjects.removeIf(po -> !po.isAlive());
    }

    private void collide(PhysicObject object){

        int start = physicObjects.indexOf(object);
        int collisionCnt = 0;
        for (int i = 0; i < physicObjects.size(); ++i){
            if (physicObjects.get(i) == object){
                continue;
            }
            Position shift = new Position(physicObjects.get(i).getPosition().sub(object.getPosition()));
            if (findCollisionCenter(object.getCollider(), physicObjects.get(i).getCollider(), shift) != null) {
                //how object influence on another object

                object.onCollision(physicObjects.get(i));
                //physicObjects.get(i).onCollision(object);

                //here physic will push out objects
                pushOut(object, physicObjects.get(i));
                collisionCnt++;
            }
        }
        if (collisionCnt == 0){
            //on least we have to collide with ground
            object.setOnGround(false);
        }
    }

    private void pushOut (PhysicObject mainObject, PhysicObject tempObject){
        //mainObject doesn't need to be pushed out
        if(!mainObject.isEjected()){
            return;
        }
        //tempObject doesn't push out
        if (!tempObject.isPushOut()){
            return;
        }

        Position shift = tempObject.getPosition().sub(mainObject.getPosition());

        double centerX = mainObject.getCollider().getWidth() / 2;
        double centerY = mainObject.getCollider().getHeight() / 2;
        Position mainObjectCenter = new Position(centerX, centerY);
        Position collisionCenter;

        int i = 0;
        while((collisionCenter = findCollisionCenter(
                mainObject.getCollider(), tempObject.getCollider(), shift)) != null &&
                i++ < GameStandardOptions.maxPushOutIterations){
            Vec2d directionToPush = normalizeVect(new Vec2d(mainObjectCenter.getX() - collisionCenter.getX(),
                    mainObjectCenter.getY() - collisionCenter.getY()));
          //  double deltaMove = (mainObjectCenter.y - collisionCenter.y) / 100;
            moveUp(directionToPush, mainObject, GameStandardOptions.deltaMove);
            changeSpeed(mainObject, directionToPush);
            shift = tempObject.getPosition().sub(mainObject.getPosition());
        }

    }

    private Vec2d normalizeVect(Vec2d v1){
        double det = Math.sqrt(v1.x * v1.x + v1.y * v1.y);
        double x = v1.x / det;
        double y = v1.y / det;
        return new Vec2d(x, y);
    }

    private void changeSpeed(PhysicObject object, Vec2d directionToPush){
        Vec2d perpendicular = new Vec2d(-directionToPush.y, directionToPush.x);
        Vec2d speedVect = new Vec2d(object.getSpeed().getVx(), object.getSpeed().getVy());
        double coef = createProjection(speedVect, perpendicular) / 1.1;
        Speed newSpeed = new Speed(perpendicular.x * coef, perpendicular.y * coef);
        object.getSpeed().setSpeed(newSpeed);
    }

    private double createProjection(Vec2d projected, Vec2d v2){
        return scalarMul(projected, v2) / Math.sqrt(v2.x * v2.x + v2.y * v2.y);
    }

    private double scalarMul(Vec2d v1, Vec2d v2){
        return v1.x * v2.x + v1.y * v2.y;
    }

    private void moveUp(Vec2d directionToMove, PhysicObject objectToMove, double deltaMove){
        double newX = objectToMove.getPosition().getX() + directionToMove.x * deltaMove;
        double newY = objectToMove.getPosition().getY() + directionToMove.y * deltaMove;
        objectToMove.setPosition(newX, newY);
    }

    private Position findCollisionCenter(PixelCollider obj1, PixelCollider obj2, Position shift){

        if (!possibleCollision(obj1, obj2, shift))
            return null;

        double centerX = 0;
        int cnt = 0;
        double centerY = 0;

        int startX = Math.max(0, (int)shift.getX());
        int endX = Math.min(obj1.getWidth(),(int) shift.getX() + obj2.getWidth());
        int startY = Math.max(0, (int) -shift.getY());
        int endY = Math.min(obj1.getHeight(), (int)-shift.getY() + obj2.getHeight());


        for (int y = startY; y < endY; ++y){
            for (int x = startX; x < endX; ++x){
                if (obj1.checkPixel(x, y) &&
                        obj2.checkPixel(x - (int)shift.getX(), y + (int)shift.getY())){
                    centerX += x;
                    centerY -= y; //!!!!!!!!!!!!!!!!!!!!!!!1
                    ++cnt;
                }
            }
        }
        return cnt > 0 ? new Position(centerX / cnt, centerY / cnt) : null;
    }

    private boolean possibleCollision(PixelCollider obj1, PixelCollider obj2, Position shift){
        if (shift.getX() > 0 && shift.getX() - obj1.getWidth() > 0) return false;
        if (shift.getX() < 0 && shift.getX() + obj2.getWidth() < 0) return false;
        if (shift.getY() > 0 && shift.getY() - obj2.getHeight() > 0) return false;
        if (shift.getY() < 0 && shift.getY() + obj1.getHeight() < 0) return  false;
        return true;
    }
}
