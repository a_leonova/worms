package ru.nsu.g16205.leonova.controllers;

import ru.nsu.g16205.leonova.GameStandardOptions;
import ru.nsu.g16205.leonova.factory.Factory;
import ru.nsu.g16205.leonova.input.InputManager;
import ru.nsu.g16205.leonova.Team;
import ru.nsu.g16205.leonova.TeamInfo;
import ru.nsu.g16205.leonova.gameObjects.AwaitableObject;
import ru.nsu.g16205.leonova.physics.Speed;
import ru.nsu.g16205.leonova.view.Timer;

import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;

public class GameController implements Controller {

    private List<Team> teams;
    private Team activeTeam;
    private double timeForStep = GameStandardOptions.timeForStep;
    private double wormTime;
    private InputManager input;
    private AwaitableObject awaitableObject;
    private boolean spaceWasEntered;
    private Timer timer = Factory.getInstance().create(Timer.class);

    public GameController(InputManager inputManager, List<TeamInfo> teamsInfo){
        input = inputManager;
        teams = new ArrayList<>(teamsInfo.size());
        for (int i = 0; i < teamsInfo.size(); ++i){
            teams.add(i, new Team(teamsInfo.get(i)));
        }
        activeTeam = teams.get(GameStandardOptions.startTeam);
        activeTeam.giveActiveWorm().active();
        awaitableObject = null;
        spaceWasEntered = false;
    }

    @Override
    public void update(double deltaTime) {

        //worm's time is up, but it didn't shoot - choose new activeWorm
        if (awaitableObject == null && wormTime + deltaTime > timeForStep){
            //takes number of active team now, and after some manipulation
            //we will have number of active team after removing dead teams
            if(teams.size() == 1){
                //TODO:WINNER!!!!!!!!!!!!!!!!!!!
            }
            int current = teams.indexOf(activeTeam);
            current -= deadTeams(current);

            activeTeam.giveActiveWorm().disactive();
            activeTeam = teams.get((current + 1) % teams.size());
            activeTeam.giveNextActiveWorm();
            activeTeam.giveActiveWorm().active();

            wormTime = 0; //update worm's time for step
            awaitableObject = wormStep(deltaTime);
        }
        //active worm has time for step
        else if (awaitableObject == null && wormTime + deltaTime < timeForStep){
            awaitableObject = wormStep(deltaTime);
        }
        //bullet just has exploded
        else if (awaitableObject != null && !awaitableObject.isAlive()){
            wormTime = 30;
            awaitableObject = null;
        }
        //bullet is flying
        else{
            //TODO:!!!!!!!!!!!!
        }
        timer.setTime((int)(timeForStep - wormTime + 1));
    }

    private AwaitableObject wormStep(double deltaTime) {
        wormTime += deltaTime;

        if (input.isJump()){
            Speed jmpSpeed = new Speed(GameStandardOptions.jumpSpeed * Math.cos(GameStandardOptions.angle),
                    GameStandardOptions.jumpSpeed * Math.sin(GameStandardOptions.angle));
            activeTeam.giveActiveWorm().jump(jmpSpeed);
        }
        else if (input.isLeft()){
            activeTeam.giveActiveWorm().move(new Speed(-GameStandardOptions.horizontalSpeed, 0));
        }
        else if (input.isRight()){
            activeTeam.giveActiveWorm().move(new Speed(GameStandardOptions.horizontalSpeed, 0));
        }
        else if (input.isSpace()){
            spaceWasEntered = true;
            if (activeTeam.giveActiveWorm().increasePower()) {
                spaceWasEntered = false;
                return activeTeam.giveActiveWorm().shoot();
            }
        }
        else if (!input.isSpace() && spaceWasEntered){
            spaceWasEntered = false;
            System.out.println("shot!");
            return activeTeam.giveActiveWorm().shoot();
        }
        else if(input.isUp()){
            activeTeam.giveActiveWorm().weaponUp();
        }
        else if (input.isDown()){
            activeTeam.giveActiveWorm().weaponDown();
        }
        else if (input.isPause()){
            //TODO:reaction
        }

        return null; //no shot
    }

    private int deadTeams(int curTeamIdx){

        int beforeDead = 0;
        int cnt = 0;
        Iterator<Team> i  = teams.iterator();

        while(i.hasNext()){
            ++cnt;
            Team team = i.next();
            if(!team.isAlive()){
                if (cnt <= curTeamIdx)
                    ++beforeDead;
                i.remove();
            }
        }
        return beforeDead;
    }
}
