package ru.nsu.g16205.leonova.controllers;

import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Screen;
import ru.nsu.g16205.leonova.GameStandardOptions;
import ru.nsu.g16205.leonova.gameObjects.Landscape;
import ru.nsu.g16205.leonova.physics.Position;
import ru.nsu.g16205.leonova.view.GraphicObject;
import ru.nsu.g16205.leonova.view.Viewer;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class GraphicController implements Controller, Viewer {

    private List<GraphicObject> trialGraphicObjects;
    private List<GraphicObject> graphicObjects;
    private List<Canvas> layers = new ArrayList<>(3);
    private List<GraphicsContext> gc3 = new ArrayList<>(3);


    private Scene scene;
    private Position camera = new Position(0.0, 0.0);

    public GraphicController(){
        trialGraphicObjects = new ArrayList<>();
        graphicObjects = new ArrayList<>();
        createScene();
    }

    @Override
    public void addGraphicObject(GraphicObject newGO) {
        trialGraphicObjects.add(newGO);
    }

    @Override
    public Scene getScene() {
        return scene;
    }

    @Override
    public void update(double deltaTime) {
        //As new graphicObject can be added during iteration
        //I put them into trial list, and every time, when updates,
        //I move it to graphicObjects list and clear trialList
        graphicObjects.addAll(trialGraphicObjects);
        trialGraphicObjects.clear();
        deleteDeadObjects();

        for(int i = 0; i < 3; ++i){
            gc3.get(i).clearRect(0,0,layers.get(i).getWidth(), layers.get(i).getHeight());
        }

        for (GraphicObject graphicObject: graphicObjects) {
            Image image = graphicObject.getImage();
            Position position = new Position(graphicObject.getPosition().getX() - camera.getX(),
                    camera.getY() - graphicObject.getPosition().getY());

            gc3.get(graphicObject.getLayer()).drawImage(image, position.getX(), position.getY());
        }

    }

    private void createScene(){

        Group groupForBattleScene = new Group();
        Rectangle2D screenBounds = Screen.getPrimary().getVisualBounds();
        scene = new Scene(groupForBattleScene, (int)screenBounds.getWidth(), (int)screenBounds.getHeight(), Color.BLACK);

        for(int i = 0; i < 3; ++i){
            layers.add(i, new Canvas(screenBounds.getWidth(), screenBounds.getHeight()));
            gc3.add(i, layers.get(i).getGraphicsContext2D());
        }

        Pane pane = new Pane();
        for(int i = 0; i < 3; ++i){
            pane.getChildren().add(layers.get(i));
        }
        groupForBattleScene.getChildren().add(pane);
    }

    private void deleteDeadObjects(){

        graphicObjects.removeIf(go -> !go.isAlive());
    }
}
