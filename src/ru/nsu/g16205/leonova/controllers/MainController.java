package ru.nsu.g16205.leonova.controllers;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Background;
import javafx.stage.Stage;
import ru.nsu.g16205.leonova.TeamInfo;
import ru.nsu.g16205.leonova.factory.Factory;
import ru.nsu.g16205.leonova.gameObjects.Landscape;
import ru.nsu.g16205.leonova.input.InputJavaFx;
import ru.nsu.g16205.leonova.input.InputManager;
import ru.nsu.g16205.leonova.physics.Physic;
import ru.nsu.g16205.leonova.physics.Position;

import java.util.List;

public class MainController  {
    private Stage stage;
    private GraphicController graphicController;
    private GameController gameController;
    private UsualPhysicController physicController;
    private InputManager inputManager;
    private List<TeamInfo> teamsInfo;
    private long lastTime;

    public MainController(Stage oStage, List<TeamInfo> oteamsInfo){
        stage = oStage;
        teamsInfo = oteamsInfo;
        inputManager = new InputJavaFx();

        graphicController = new GraphicController();
        Factory.getInstance().setViewer(graphicController);

        physicController = new UsualPhysicController();
        Factory.getInstance().setPhysic(physicController);

        gameController = new GameController(inputManager, teamsInfo);
        createBattleScene();
        createGraphicObjects();
        lastTime = System.currentTimeMillis();
    }

    public void update()  {
        long newTime = System.currentTimeMillis();
        double millsDeltaTime = 30 * 0.001;
        graphicController.update(millsDeltaTime);
        gameController.update(millsDeltaTime);
        physicController.update(millsDeltaTime);
    }

    private void createBattleScene(){
        Scene battleScene = graphicController.getScene();
        battleScene.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.RIGHT){
                inputManager.setRight(true);
            }
            else if (event.getCode() == KeyCode.LEFT){
                inputManager.setLeft(true);
            }
            else if (event.getCode() == KeyCode.UP){
                inputManager.setWeaponUp(true);
            }
            else if (event.getCode() == KeyCode.DOWN){
                inputManager.setWeaponDown(true);
            }
            else if (event.getCode() == KeyCode.SPACE){
                inputManager.setSpace(true);
            }
            else if (event.getCode() == KeyCode.ESCAPE){
                inputManager.setPause(true);
            }
            else if (event.getCode() == KeyCode.ENTER){
                inputManager.setJump(true);
            }
        });
        battleScene.setOnKeyReleased(event -> {
            if (event.getCode() == KeyCode.RIGHT){
                inputManager.setRight(false);
            }
            else if (event.getCode() == KeyCode.LEFT){
                inputManager.setLeft(false);
            }
            else if (event.getCode() == KeyCode.UP){
                inputManager.setWeaponUp(false);
            }
            else if (event.getCode() == KeyCode.DOWN){
                inputManager.setWeaponDown(false);
            }
            else if (event.getCode() == KeyCode.SPACE){
                inputManager.setSpace(false);
            }
            else if (event.getCode() == KeyCode.ESCAPE){
                inputManager.setPause(false);
            }
            else if (event.getCode() == KeyCode.ENTER){
                inputManager.setJump(false);
            }
        });
        stage.setScene(battleScene);
    }

    private void createGraphicObjects(){
        Factory.getInstance().create(Landscape.class, new Position(0, 0));
    }
}
