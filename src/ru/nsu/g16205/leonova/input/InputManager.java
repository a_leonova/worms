package ru.nsu.g16205.leonova.input;

public interface InputManager {

    void setJump(boolean jump);
    void setRight(boolean right);
    void setLeft(boolean left);
    void setSpace(boolean space);
    void setWeaponUp(boolean weaponUp);
    void setWeaponDown(boolean weaponDown);
    void setPause(boolean pause);



    boolean isJump();
    boolean isRight();
    boolean isLeft();
    boolean isSpace();
    boolean isPause(); //esc was pressed
    boolean isUp();
    boolean isDown();

}
