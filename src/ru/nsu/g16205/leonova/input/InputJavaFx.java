package ru.nsu.g16205.leonova.input;

import javafx.scene.Scene;

public class InputJavaFx implements InputManager {

    private boolean jump;
    private boolean right;
    private boolean left;
    private boolean space;
    private boolean weaponUp;
    private boolean weaponDown;
    private boolean pause;


    public  InputJavaFx(){    }

    public void setJump(boolean jump) {
        this.jump = jump;
    }

    public void setRight(boolean right) {
        this.right = right;
    }

    public void setLeft(boolean left) {
        this.left = left;
    }

    public void setSpace(boolean space) {
        System.out.println("space was entered");
        this.space = space;
    }

    public void setWeaponUp(boolean weaponUp) {
        this.weaponUp = weaponUp;
    }

    public void setWeaponDown(boolean weaponDown) {
        this.weaponDown = weaponDown;
    }

    public void setPause(boolean pause) {
        this.pause = pause;
    }

    @Override
    public boolean isJump() {
        return jump;
    }

    @Override
    public boolean isRight() {
        return right;
    }

    @Override
    public boolean isLeft() {
        return left;
    }

    @Override
    public boolean isSpace() {
        return space;
    }

    @Override
    public boolean isPause() {
        return pause;
    }

    @Override
    public boolean isUp() {
        return weaponUp;
    }

    @Override
    public boolean isDown() {
        return weaponDown;
    }
}
