package ru.nsu.g16205.leonova.physics;

import javafx.geometry.Pos;

import java.util.List;

public interface PhysicObject {

    void onCollision(PhysicObject physicObjects);
    PixelCollider getCollider();
    Position getPosition();
    default Position getCenter(){
        double newX = getPosition().getX() + getCollider().getWidth() / 2;
        double newY = getPosition().getY() - getCollider().getHeight()/2;
        return new Position(newX, newY);
    }
    Speed getSpeed();
    void oneIterationGone();
    boolean isGravity();
    boolean isAlive();
    boolean isPushOut();
    boolean isEjected();
    boolean onGround();
    boolean isMoving();
    void setOnGround(boolean value);
    void setPosition(Position newPosition);
    void setPosition(double x, double y);

}
