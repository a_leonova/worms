package ru.nsu.g16205.leonova.physics;

import ru.nsu.g16205.leonova.GameStandardOptions;

public class Speed {
    private double vx;
    private double vy;

    public Speed(){
        vx = 0.0;
        vy = 0.0;
    }

    public Speed(double newVx, double newVy){
        vx = newVx;
        vy = newVy;
    }

    public Speed(Speed newSpeed){
        vx = newSpeed.vx;
        vy = newSpeed.vy;
    }

    public double getVx() {
        return vx;
    }

    public double getVy() {
        return vy;
    }

    public void setVx(double vx) {
        this.vx = vx;
    }

    public void setVy(double vy) {
        this.vy = vy;
    }

    public void setSpeed(Speed newSpeed){
        vx = newSpeed.vx;
        vy = newSpeed.vy;
    }

    public Speed mull(double scalar){
        Speed res = new Speed();
        res.vx = vx * scalar;
        res.vy = vy * scalar;
        return res;
    }
}
