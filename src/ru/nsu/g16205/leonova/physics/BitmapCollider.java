package ru.nsu.g16205.leonova.physics;

import com.sun.org.apache.xpath.internal.operations.Bool;
import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.min;

public class BitmapCollider implements PixelCollider {

    private List<Boolean> showedPixel;
    private int height;
    private int width;

    public BitmapCollider(Image image){
        height = (int) image.getHeight();
        width = (int) image.getWidth();

        showedPixel = new ArrayList<Boolean>(height * getWidth());

        for (int y = 0; y < height; ++y){
            for (int x = 0; x < width; ++x){
                PixelReader reader =  image.getPixelReader();
                int pixel = reader.getArgb(x, y);
                showedPixel.add(y * width + x, (pixel >> 24 & 0xFF) != 0x00);
            }
        }
        int i = 0;
        ++i;
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public boolean checkPixel(int x, int y) {
        int pixel = y * width + x;
        return showedPixel.get(pixel);
    }

    @Override
    public void setPixel(int x, int y, boolean value) {
        int pixel = y * width + x;
        showedPixel.set(pixel, value);
    }
}
