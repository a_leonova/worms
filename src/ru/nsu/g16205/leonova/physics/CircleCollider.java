package ru.nsu.g16205.leonova.physics;

import java.util.ArrayList;
import java.util.List;

public class CircleCollider implements PixelCollider {

    private int radius;
    private List<Boolean> collider;

    public CircleCollider(int r){
        radius = r;
        collider = new ArrayList<>(4 * radius * radius); //(2*radius)*2(sides)
        createCircle();
    }

    private void createCircle(){
        for (int y = -radius ; y < radius; ++y){
            for (int x = -radius; x < radius; ++x){
                boolean isCircle = (x*x + y*y <= radius*radius);
                collider.add(isCircle);
            }
        }
    }

    @Override
    public int getWidth() {
        return radius * 2;
    }

    @Override
    public int getHeight() {
        return radius * 2;
    }

    @Override
    public boolean checkPixel(int x, int y)
    {   int pixel = y * radius * 2 + x;
        return collider.get(pixel);
    }

    @Override
    public void setPixel(int x, int y, boolean value) {
        int pixel = y * radius * 2 + x;
        collider.set(pixel, value);
    }
}
