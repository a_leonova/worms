package ru.nsu.g16205.leonova.physics;

import javafx.geometry.Pos;

public class Position {
   private double x;
   private double y;

   public Position(){
       x = 0;
       y = 0;
   }

   public Position(double newX, double newY){
       x = newX;
       y = newY;
   }

   public Position(Position otherPosition){
       x = otherPosition.x;
       y = otherPosition.y;
   }

   public void setPosition(double newX, double newY){
       x = newX;
       y = newY;
   }

    public void setPosition(Position otherPosition){
        x = otherPosition.x;
        y = otherPosition.y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public Position add(Position pos2){
       return new Position(x + pos2.x, y + pos2.y);
   }

   public Position sub (Position pos2){
       return new Position(x - pos2.x, y - pos2.y);
   }

   public boolean equal(Position pos2){
       return x == pos2.x && y == pos2.y;
   }
}
