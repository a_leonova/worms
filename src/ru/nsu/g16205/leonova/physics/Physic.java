package ru.nsu.g16205.leonova.physics;

import java.util.List;

public interface Physic {

    void addPhysicObj(PhysicObject newObject);
    List<PhysicObject> getPhysicObj();
    void update(double deltaTime);


}
