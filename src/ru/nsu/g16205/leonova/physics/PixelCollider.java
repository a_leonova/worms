package ru.nsu.g16205.leonova.physics;

public interface PixelCollider {

    int getWidth();
    int getHeight();
    boolean checkPixel(int x, int y);
    void setPixel(int x, int y, boolean value);

}
