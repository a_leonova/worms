package ru.nsu.g16205.leonova;

import javafx.animation.AnimationTimer;
import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Rectangle2D;
import javafx.scene.Group;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Screen;
import ru.nsu.g16205.leonova.controllers.GameController;
import ru.nsu.g16205.leonova.controllers.GraphicController;
import ru.nsu.g16205.leonova.controllers.MainController;
import ru.nsu.g16205.leonova.factory.Factory;
import ru.nsu.g16205.leonova.gameObjects.Landscape;
import ru.nsu.g16205.leonova.input.InputJavaFx;
import ru.nsu.g16205.leonova.input.InputManager;
import ru.nsu.g16205.leonova.controllers.UsualPhysicController;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import ru.nsu.g16205.leonova.physics.Position;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class Main extends  Application{

    private List<TeamsVBox> listTeamInfoVBox;
    private ArrayList<TeamInfo> teamsInfo;
    private Stage stage;

    public static void main(String[] args){
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        stage = primaryStage;
        primaryStage.setTitle("Worms");
        //graphicController creates scene by his own
        //here we take this scene
        Scene startScene = createStartScene();
        primaryStage.setScene(startScene);
        primaryStage.show();
    }

    private Scene createStartScene(){
        Group group = new Group();
        Scene startScene = new Scene(group);
        Rectangle2D screenBounds = Screen.getPrimary().getVisualBounds();
        Canvas canvas = new Canvas(screenBounds.getWidth(), screenBounds.getHeight());
        Image background = new Image(("start.jpg"));
        canvas.getGraphicsContext2D().drawImage( background, 0,0, screenBounds.getWidth(), screenBounds.getHeight());
        group.getChildren().add(canvas);

        BorderPane border = new BorderPane();

        GridPane fieldForTeamInfo = new GridPane();
        fieldForTeamInfo.setPadding(new Insets(50, 5, 5, 50));

        final ComboBox<String> teamsNumber = createWormsNumberComboBox(fieldForTeamInfo);

        HBox QuestionNumTeam = new HBox();
        QuestionNumTeam.setPadding(new Insets(15, 12, 15, 12));
        QuestionNumTeam.setSpacing(10);
        QuestionNumTeam.getChildren().addAll(new Label("How many teams: "), teamsNumber);
        border.setTop(QuestionNumTeam);
        teamsInfo = new ArrayList<>(2);
        draw(2, fieldForTeamInfo);


        border.setCenter(fieldForTeamInfo);

        Button start = new Button("Start");

        start.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                int i = 0;
                System.out.println("aaa");
                for (TeamsVBox teamInfo: listTeamInfoVBox){
                    teamsInfo.add(new TeamInfo(teamInfo.getWormsNumber(),
                            teamInfo.getTeamName(), teamInfo.getWormsLives(), teamInfo.getColor()));
                    ++i;
                }

                System.out.println("Didn't break!!!");
                MainController mainController = new MainController(stage, teamsInfo);


                final LongProperty lastUpdateTime = new SimpleLongProperty(0);
                final AnimationTimer timer = new AnimationTimer() {
                    @Override
                    public void handle(long timestamp) {
                        if (lastUpdateTime.get() > 0) {
                            long elapsedTime = timestamp - lastUpdateTime.get();
                            mainController.update();
                        }
                        lastUpdateTime.set(timestamp);
                    }

                };
                timer.start();
            }
        });

        border.setBottom(start);
        group.getChildren().add(border);
        return startScene;
    }

    private ComboBox<String> createWormsNumberComboBox(GridPane fieldForTeamInfo){
        final ComboBox<String> teamsNumber = new ComboBox<String>();
        teamsNumber.getItems().addAll("2", "3", "4", "5", "6", "7", "8", "9", "10");
        teamsNumber.setPromptText("2");
        teamsNumber.valueProperty().addListener(new ChangeListener<String>() {
            @Override public void changed(ObservableValue ov, String oldVal, String newVal) {
                System.out.println(newVal);
                teamsInfo = new ArrayList<>(Integer.valueOf(newVal));
                draw(Integer.parseInt(newVal), fieldForTeamInfo);
            }
        });
        return teamsNumber;
    }

    private void draw(int number, GridPane grid){
        grid.getChildren().clear();

        listTeamInfoVBox = new ArrayList<>();
        for (int i = 0; i < number; i++) {
            listTeamInfoVBox.add(new TeamsVBox());
        }

        ListView<TeamsVBox> listView = new ListView<>();
        ObservableList<TeamsVBox> myObservableList = FXCollections.observableList(listTeamInfoVBox);
        listView.setItems(myObservableList);

        grid.add(listView, 0, 0);
    }
}
