package ru.nsu.g16205.leonova;


import javafx.scene.paint.Color;

public class TeamInfo {
    int cnt;
    String name;
    int lives;
    private Color color;

    public TeamInfo(int oCnt, String oName, int oLive, Color oColor){
        cnt = oCnt;
        name = oName;
        lives = oLive;
        color = oColor;
    }

    public int getCnt() {
        return cnt;
    }

    public String getName() {
        return name;
    }

    public int getLives() {
        return lives;
    }

    public Color getColor(){return color;}
}
