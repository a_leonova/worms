package ru.nsu.g16205.leonova.factory;

import ru.nsu.g16205.leonova.controllers.GraphicController;
import ru.nsu.g16205.leonova.physics.Physic;
import ru.nsu.g16205.leonova.physics.PhysicObject;
import ru.nsu.g16205.leonova.view.GraphicObject;
import ru.nsu.g16205.leonova.view.Viewer;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.List;

public class Factory {

    private static Factory instance;
    private Physic physic;
    private Viewer viewer;

    public static Factory getInstance(){
        Factory localInstance = instance;

        if (localInstance == null){
            synchronized (Factory.class){
                localInstance = instance;
                if (localInstance == null){
                    instance = localInstance = new Factory();
                }
            }
        }
        return localInstance;
    }

    public void setPhysic(Physic newPhysic){
        physic = newPhysic;
    }
    public void setViewer(Viewer newViewer) {
        viewer = newViewer;
    }

    public <T> T create(Class<T> targetClass, Object... args){
        try {
            if (physic == null){
                throw new NullPointerException("Where is my physic??");
            }
            if (viewer == null){
                throw new NullPointerException("Where is my viewer??");
            }

            Class argumentTypes[] = new Class[args.length];
            for (int i = 0; i < args.length; ++i) {
                argumentTypes[i] = args[i].getClass();
            }

            T object = null;
            Constructor<?>[] constructors = targetClass.getConstructors();
            for (Constructor ctor : constructors){
                Class<?>[] pType = ctor.getParameterTypes();
                if(pType.length != argumentTypes.length)
                    continue;

                boolean isMatch = true;
                for (int i = 0; i < pType.length; i++) {
                    if (!pType[i].isAssignableFrom(argumentTypes[i])) {
                        isMatch = false;
                        break;
                    }
                }
                if(!isMatch){
                    continue;
                }
                object = (T)ctor.newInstance(args);
                break;
            }

            if(object == null){
                throw new NoSuchMethodException();
            }

            if (object instanceof PhysicObject){
                physic.addPhysicObj((PhysicObject) object);
            }

            if (object instanceof GraphicObject){
                viewer.addGraphicObject((GraphicObject) object);
            }

            return object;
        } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            e.printStackTrace();
            System.exit(-1);
        }
        return null; //java -_-
    }

}
