package ru.nsu.g16205.leonova.view;

import javafx.scene.image.Image;
import ru.nsu.g16205.leonova.physics.Position;

public interface GraphicObject {
    Position getPosition();
    int getWidth();
    int getHeight();
    Image getImage();
    int getLayer();
    boolean isRight();
    boolean isAlive();
}
