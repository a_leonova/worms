package ru.nsu.g16205.leonova.view;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;

import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;

public class ImageTransformation {

    public static Image flip(Image imageToTransform){
        BufferedImage buffImg= SwingFXUtils.fromFXImage(imageToTransform, null);
        AffineTransform tx = AffineTransform.getScaleInstance(-1,1);
        tx.translate(-buffImg.getWidth(null), 0);
        AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
        buffImg = op.filter(buffImg, null);
        imageToTransform = SwingFXUtils.toFXImage(buffImg, null);
        return  imageToTransform;
    }

    public static Image changeAngle(Image imageToTransform, double angleRad){
        BufferedImage buffImg= SwingFXUtils.fromFXImage(imageToTransform, null);
        double locationX = imageToTransform.getWidth() / 2;
        double locationY = imageToTransform.getHeight() / 2;
        AffineTransform tx = AffineTransform.getRotateInstance(angleRad, locationX, locationY);
        AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_BILINEAR);
        buffImg = op.filter(buffImg, null);

        imageToTransform = SwingFXUtils.toFXImage(buffImg, null);
        return  imageToTransform;
    }


}
