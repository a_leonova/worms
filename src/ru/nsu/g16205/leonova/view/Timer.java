package ru.nsu.g16205.leonova.view;

import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import ru.nsu.g16205.leonova.GameStandardOptions;
import ru.nsu.g16205.leonova.physics.Position;

public class Timer implements GraphicObject {

    private int time = 30;
    private Position pos = new Position(GameStandardOptions.timerPosX, GameStandardOptions.timerPosY);


    @Override
    public Position getPosition() {
        return pos;
    }

    @Override
    public int getWidth() {
        return 0;
    }

    @Override
    public int getHeight() {
        return 0;
    }

    @Override
    public Image getImage() {
        return textToImage(Integer.toString(time));
    }

    @Override
    public int getLayer() {
        return 2;
    }

    @Override
    public boolean isRight() {
        return true;
    }

    @Override
    public boolean isAlive() {
        return true;
    }


    private WritableImage textToImage(String text){

        SnapshotParameters sp = new SnapshotParameters();
        sp.setFill(Color.TRANSPARENT);

        Text t = new Text(text);
        t.setFill(Color.WHITE);
        t.setFont(Font.loadFont("file:resource/fonts/Amiri-Bold.ttf", 50));
        Scene scene = new Scene(new StackPane(t));
        return t.snapshot(sp, null);

    }

    public void setTime(int time){
        this.time = time;
    }
}
