package ru.nsu.g16205.leonova.view;

import javafx.scene.Scene;

public interface Viewer {
    void addGraphicObject(GraphicObject newGO);
    Scene getScene();
}
