package ru.nsu.g16205.leonova;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.*;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.SVGPath;

public class TeamsVBox extends VBox {

    private Label teamName;
    private TextField answerTeamName;
    private Label wormsNumber;
    private Spinner<Integer> answerWormsNumber;
    private Label wormsLives;
    private Spinner<Integer> answerWormsLives;
    private GridPane grid;

    private HBox nameInfo = new HBox();
    private HBox numberInfo = new HBox();
    private HBox livesInfo = new HBox();
    private ColorPicker colorPicker = new ColorPicker();

    public TeamsVBox(){
        super();

        teamName = new Label("Team's name");
        answerTeamName = new TextField("");
        wormsNumber = new Label("Quantity of worms");
        answerWormsNumber = new Spinner<>();
        answerWormsNumber.setEditable(true);
        wormsLives = new Label("Number of lives");
        answerWormsLives = new Spinner<>();
        answerWormsLives.setEditable(true);

        nameInfo.getChildren().addAll(teamName, answerTeamName);
        numberInfo.getChildren().addAll(wormsNumber, answerWormsNumber);
        livesInfo.getChildren().addAll(wormsLives, answerWormsLives);
        final SVGPath svg = new SVGPath();
        svg.setContent("M70,50 L90,50 L120,90 L150,50 L170,50"
                + "L210,90 L180,120 L170,110 L170,200 L70,200 L70,110 L60,120 L30,90"
                + "L70,50");
        svg.setStroke(Color.DARKGREY);
        svg.setStrokeWidth(2);
        svg.setEffect(new DropShadow());
        svg.setFill(colorPicker.getValue());

        colorPicker.setOnAction(new EventHandler() {
            public void handle(Event t) {
                svg.setFill(colorPicker.getValue());
            }
        });

        SpinnerValueFactory<Integer> valueFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory
                (1, 20, 1);

        SpinnerValueFactory<Integer> valueFactory1 = new SpinnerValueFactory.IntegerSpinnerValueFactory
                (1, 20, 1);
        answerWormsLives.setValueFactory(valueFactory);
        answerWormsNumber.setValueFactory(valueFactory1);
        getChildren().addAll(nameInfo, numberInfo, livesInfo, colorPicker);
    }

    public Integer getWormsNumber(){
        String text = answerWormsNumber.getEditor().getText();
        return Integer.valueOf(text);
    }

    public Integer getWormsLives(){
        String text = answerWormsLives.getEditor().getText();
        return Integer.valueOf(text);
    }

    public String getTeamName(){
        return answerTeamName.getText();
    }

    public Color getColor(){
        return colorPicker.getValue();
    }


}
