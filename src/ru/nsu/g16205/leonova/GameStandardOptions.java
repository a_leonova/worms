package ru.nsu.g16205.leonova;

import javafx.geometry.Rectangle2D;
import javafx.stage.Screen;
import ru.nsu.g16205.leonova.physics.Speed;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class GameStandardOptions {
    public static int stdUsualBulletDamage = 20;
    public static double speedForBullet = 200;
    public static int airBlastRad = 45;
    public static double timeForStep = 30;
    public static int startTeam = 0;
    public static double powerEnlargement = 0.01;
    public static double maxShotPower = 1.0;


    public static double gameWidth = Screen.getPrimary().getVisualBounds().getWidth();
    public static double gameHeight = Screen.getPrimary().getVisualBounds().getHeight();
    public static double startWormsHeight = 100;
    public static double gravityConst = 120.98;

    public static double horizontalSpeed = 20;

    public static Speed rightSpeed = new Speed(horizontalSpeed   , 0);
    public static Speed leftSpeed = new Speed(-horizontalSpeed, 0);
    public static double jumpSpeed = 100;
    public static double angle = Math.toRadians(70);


    //public static double frictionForce = 0.001 * rightSpeed.vx;
    public static double speedToStopSqr = Math.pow(gravityConst*0.5, 2) + horizontalSpeed * horizontalSpeed;
    public static double deltaMove = 0.5;

    public static int maxPushOutIterations = 10;

    public static double weaponXShift = 2.0/3.0;
    public static double weaponYShift = 3.0/4.0;

    public static double distFromcenterToLanch= 50.0;

    public static double shiftYForInfo = 20.0;
    public static int duration = 2;

    public static double timerPosX = Screen.getPrimary().getVisualBounds().getWidth() / 2 - 50;
    public static double timerPosY = -30;

}
